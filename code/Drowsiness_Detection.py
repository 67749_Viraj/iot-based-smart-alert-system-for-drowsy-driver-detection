from scipy.spatial import distance
from imutils import face_utils
import imutils
import dlib
import cv2
import RPi.GPIO as GPIO
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
alco=21# Use BCM GPIO numbers
relay=20
buz=26
GPIO.setup(alco, GPIO.IN)
GPIO.setup(relay, GPIO.OUT)
GPIO.setup(buz, GPIO.OUT)
GPIO.output(relay, True)
GPIO.output(buz, False)
GPIO.output(relay, False)
import serial
import pynmea2
#from geopy.geocoders import Nominatim
import utm
import time
import glob
import time
#locs='_,_'
global locs
#geolocator = Nominatim()
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from array import *
def mailt(msgs):    
    fromaddr = "sur5gk@gmail.com"
    toaddr = "samirdagade99@gmail.com"    
    msg = MIMEMultipart()
    msg['From']=fromaddr
    msg['To']=toaddr
    msg['Subject']="Emergency"
    body = msgs    
    msg.attach(MIMEText(body,'plain'))    
    part = MIMEBase('application','octet-stream')    
    server=smtplib.SMTP('smtp.gmail.com',587)
    server.starttls()
    server.login(fromaddr,"fjum jpxg zgeh qfjj")
    text=msg.as_string()
    server.sendmail(fromaddr,toaddr,text)
    server.quit()
def mailt1(msgs):    
    fromaddr = "sur5gk@gmail.com"
    toaddr = "virajsawant67@gmail.com"    
    msg = MIMEMultipart()
    msg['From']=fromaddr
    msg['To']=toaddr
    msg['Subject']="Emergency"
    body = msgs    
    msg.attach(MIMEText(body,'plain'))    
    part = MIMEBase('application','octet-stream')    
    server=smtplib.SMTP('smtp.gmail.com',587)
    server.starttls()
    server.login(fromaddr,"fjum jpxg zgeh qfjj")
    text=msg.as_string()
    server.sendmail(fromaddr,toaddr,text)
    server.quit()
serialPort = serial.Serial("/dev/ttyAMA0", 9600, timeout=0.5)
def parseGPS(str):
    global locs
    if str.find('GGA') > 0:
        msg = pynmea2.parse(str)
        #print "Timestamp: %s -- Lat: %s %s -- Lon: %s %s -- Altitude: %s %s" % (msg.timestamp,msg.lat,msg.lat_dir,msg.lon,msg.lon_dir,msg.altitude,msg.altitude_units)
     
        lat1=int(msg.lat[0:2])
        lat2=float(msg.lat[2:9])
     
        lat2=lat2/60
        lat=lat1+lat2
       # print msg.lon
        if int(msg.lon[0:1])==int(0):            
            lon1=int(msg.lon[0:3])
            lon2=float(msg.lon[3:11])
           # print lon2
            lon2=lon2/60
            lon=lon1+lon2
        else:
            lon1=int(msg.lon[0:2])
            lon2=float(msg.lon[2:10])
          #  print lon2
            lon2=lon2/60
            lon=lon1+lon2
        print (lat,lon)
        locs=str(lat)+str(lon)
        #var=("%s,%s" % (lat,lon))  # Message
        #location = geolocator.reverse(var)
        #msg=(location.address)
        #print (msg)
       
def eye_aspect_ratio(eye):
	A = distance.euclidean(eye[1], eye[5])
	B = distance.euclidean(eye[2], eye[4])
	C = distance.euclidean(eye[0], eye[3])
	ear = (A + B) / (2.0 * C)
	return ear
	
thresh = 0.25
frame_check = 10
detect = dlib.get_frontal_face_detector()
predict = dlib.shape_predictor("models/shape_predictor_68_face_landmarks.dat")# Dat file is the crux of the code

(lStart, lEnd) = face_utils.FACIAL_LANDMARKS_68_IDXS["left_eye"]
(rStart, rEnd) = face_utils.FACIAL_LANDMARKS_68_IDXS["right_eye"]
cap=cv2.VideoCapture(0)
flag=0
#mailt('alcohol detect')
locs='18.585890,74.002167'
while True:
    #global locs
    str=serialPort.readline()
    #parseGPS(str)
    if(not GPIO.input(alco)):
        print('Alcohol')
        GPIO.output(buz, True)
        GPIO.output(relay, True)
        mailt('alcohol detect:'+locs)
        mailt1('alcohol detect:'+locs)
        time.sleep(2)
        GPIO.output(buz, True)
        GPIO.output(relay, False)
   
    ret, frame=cap.read()
    
    frame = imutils.resize(frame, width=450)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    subjects = detect(gray, 0)
    for subject in subjects:
        shape = predict(gray, subject)
        shape = face_utils.shape_to_np(shape)#converting to NumPy Array
        leftEye = shape[lStart:lEnd]
        rightEye = shape[rStart:rEnd]
        leftEAR = eye_aspect_ratio(leftEye)
        rightEAR = eye_aspect_ratio(rightEye)
        ear = (leftEAR + rightEAR) / 2.0
        leftEyeHull = cv2.convexHull(leftEye)
        rightEyeHull = cv2.convexHull(rightEye)
        cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
        cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)
        if ear < thresh:
            flag += 1
            print (flag)
            if flag >= frame_check:
                flag=0
                cv2.putText(frame, "****************ALERT!****************", (10, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
                cv2.putText(frame, "****************ALERT!****************", (10,325),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
                GPIO.output(relay, True)
                GPIO.output(buz, True)
                mailt('Drawsiness detect:'+locs)
                mailt1('alcohol detect:'+locs)
                print('Drawsiness detect')
                time.sleep(2)
                GPIO.output(buz, True)
                GPIO.output(relay, False)
                
                #print ("Drowsy")
        else:
            flag = 0
    cv2.imshow("Frame", frame)
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break
cv2.destroyAllWindows()
cap.release() 
